.PHONY: all clean

CXXFLAGS=-g -std=c++11 -Wall -pedantic -pthread

all: ais

ais: main.o
	$(LINK.cc) -o $@ $^

clean:
	rm -f *.o
	rm ais
