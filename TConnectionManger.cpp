#include "TConnectionManger.h"

#include <iostream>       // std::cout
#include <thread>         // std::thread, std::this_thread::sleep_for

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

namespace {
static const int PORTNO = 5555;
static const int OPT_VAL = 1;
}

TConnectionManger::TConnectionManger()
  : ListenSockFd(-1)
{
  ListenSockFd = ::socket(AF_INET, SOCK_STREAM, 0);
  if (ListenSockFd < 0) {
    throw std::exception("Unable to open socket");
  }

  ::setsockopt(ListenSockFd, SOL_SOCKET, SO_REUSEADDR, &OPT_VAL,
    sizeof(OPT_VAL));

  struct sockaddr_in serv_addr;
  ::bzero((char *) &serv_addr, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(PORTNO);
  if (::bind(ListenSockFd, (struct sockaddr *) &serv_addr, sizeof(serv_addr))
    < 0) {
    throw std::exception("Unable to bind socket");
  }

  ::listen(ListenSockFd, 5);
}

TConnectionManger::~TConnectionManger()
{
  ::close(ListenSockFd);
}
