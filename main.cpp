#include <iostream>       // std::cout
#include <thread>         // std::thread, std::this_thread::sleep_for

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

static const int PORTNO = 5555;
static const int OPT_VAL = 1;

void handle_connection(int newsockfd)
{
  while (true) {
    std::cout << "(" << syscall(SYS_gettid) << "): conn_thread: read from "
      << newsockfd << std::endl;
    char buffer[256];
    ::bzero(buffer, 256);
    int n = ::read(newsockfd, buffer, 255);
    if (n < 0) perror("ERROR reading from socket");
    std::cout << "(" << syscall(SYS_gettid) << "): Here is the message from "
      << newsockfd << ": " << buffer;
    n = ::write(newsockfd, "I got your message\n", 19);
    if (n < 0) perror("ERROR writing to socket");
  }
}

void listen_thread()
{
  int sockfd = ::socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0) {
    ::perror("ERROR opening socket");
    exit(1);
  }

  ::setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &OPT_VAL, sizeof(OPT_VAL));

  struct sockaddr_in serv_addr;
  ::bzero((char *) &serv_addr, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(PORTNO);
  if (::bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
    ::perror("ERROR on binding");
    exit(1);
  }

  ::listen(sockfd, 5);

  std::cout << "(" << syscall(SYS_gettid)
    << "): Waiting for incoming connetions...\n";
  while (true) {
    struct sockaddr_in cli_addr;
    socklen_t clilen = sizeof(cli_addr);
    int newsockfd = ::accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
    if (newsockfd < 0) {
      perror("ERROR on accept");
    }
    std::thread(handle_connection, newsockfd).detach();
  }
}

int main()
{
  std::thread t(listen_thread);
  t.join();
  std::cout << "All threads joined!\n";

  return 0;
}

